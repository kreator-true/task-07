package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static DBManager instance;
	private String url;

	public static synchronized DBManager getInstance() {
		return instance == null ? instance = new DBManager() : instance;
	}


	private DBManager() {
		Properties prop = new Properties();
		try (FileReader fileReader = new FileReader("app.properties")){
			prop.load(fileReader);
			url = prop.getProperty("connection.url");
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(url);
			 Statement statement = connection.createStatement();
			 ResultSet resultSet = statement.executeQuery("select * from users")){
			while(resultSet.next()){
				users.add(new User(resultSet.getInt(1), resultSet.getString(2)));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("insert into users(login) values(?) ")){
			statement.setString(1, user.getLogin());
			return statement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}

	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean isDelete = false;
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("delete from users where login = ?")){
			for(User user : users) {
				statement.setString(1, user.getLogin());
				isDelete = statement.executeUpdate() > 0 && deleteFromUsersTeams(connection, user.getId(), "user");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isDelete;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("select * from users where login = ?")){

			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next())
				user = new User(resultSet.getInt(1), resultSet.getString(2));

			resultSet.close();

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("select * from teams where name = ?")){
			statement.setString(1, name);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) team = new Team(resultSet.getInt(1), resultSet.getString(2));
			}catch (SQLException ex){
				throw new DBException(ex.getMessage(), ex.getCause());
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(url);
			 Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from teams")){
			while(resultSet.next()){
				teams.add(new Team(resultSet.getInt(1), resultSet.getString(2)));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("insert into teams(name) values(?)")){
			statement.setString(1, team.getName());
			return statement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		user = getUser(user.getLogin());
		for (int i = 0; i < teams.length; i++) {
			teams[i] = getTeam(teams[i].getName());
		}
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("insert into users_teams values(?, ?)")){

			for(Team team : teams) {
				statement.setInt(1, user.getId());
				statement.setInt(2, team.getId());
				statement.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		List<Integer> teams_id = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(url);){
			PreparedStatement prepareStatement = connection.prepareStatement("select team_id from users_teams where user_id = ?");

			prepareStatement.setInt(1, user.getId());
			ResultSet resultSet = prepareStatement.executeQuery();
				while (resultSet.next()) {
					teams_id.add(resultSet.getInt(1));
				}
				resultSet.close();
				prepareStatement.close();

				Statement statement = connection.createStatement();
				for (int id : teams_id) {
					resultSet = statement.executeQuery("select * from teams where id = " + id);

					if (resultSet.next())
						teams.add(new Team(resultSet.getInt(1), resultSet.getString(2)));
					resultSet.close();
				}

				statement.close();

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("delete from teams where name = ?")){

			statement.setString(1, team.getName());
			return statement.executeUpdate() > 0 && deleteFromUsersTeams(connection, team.getId(), "team");

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement("update teams set name = ? where id = ?")){

			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			return statement.executeUpdate() > 0;

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	private boolean deleteFromUsersTeams(Connection connection, int id, String entity) throws DBException{
		try(Statement statement = connection.createStatement()){
			return statement.executeUpdate(String.format("delete from users_teams where %s = %d", entity.concat("_id"), id)) > 0;
		}catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}



}
